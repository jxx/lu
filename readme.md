# Lu

A sample project that gets the search suggest from amazon.

## How to use
<ol>
<li>Start by cloning the repo. </li>
<li>Run <code>composer install.</code></li>
<li><code>cp .env.example .env</code> and put your database credentials.</li>
<li>Run <code>php artisan migrate</code> to set up the tables.</li>
<li>Run <code>php artisan search:amazon</code></li>

## Additional info
I'm using sequel pro so it limits the result to only 1000 records. To adjust it
go to:

<h6>Preferences > Tables > Limit result to</h6>

